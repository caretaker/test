import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Combinator {
    //Предметы с общим принципом деления и наменования
    static ArrayList<String> subjectNames = new ArrayList<>(Arrays.asList("Р","Л","И","М","Е","ПС","Фгр","Г","О","П","МХК","Х","Б","Астр","Ф","Инф","(АВ2+АВ3)","(А2+А5)","Фр+Нем","АВ1+АВ4","АВ1+2Инф","1Инф+АВ4","(А1+А6)","(А3+А4)"));
    //Порядковые номера предметов с возможность совместного проведения у 3 и 4 группы
    static ArrayList<Integer> duoSubjectNumbers = new ArrayList(Arrays.asList(0,1,2,3,6,7,8,13));
    //Порядковые номера предметов с возможность совместного проведения у 2, 3 и 4 группы
    static ArrayList<Integer> trioSubjectNumbers = new ArrayList<>(Arrays.asList(0,1,3,6,7,8));
    //Матрица отображает количество часов по предмету у группы. Для каждой группы свой массив.
    //Англ язык и английский язык по выбору, только для полностью самостоятельной группы 1.
    //Информатика только для самостоятельных групп 3 и 4
    //Всё что правее Английского для первой группы используется для генерации полных сетов и не используется при создании списка возможных комбинаций
                           //0 1 2 3 4 5  6   7 8 9 10 11 12 13 14 15  16    17   18     19      20       21       22    23
                           //Р,Л,И,М,Е,ПС,Фгр,Г,О,П,МХК,Х,Б,Астр,Ф,Инф,АВ2+3,А2+5,Фр+Нем,АВ1+АВ4,АВ1+2Инф,1Инф+АВ4,А1+А6,А3+А4
    static byte[][] hours ={{3,5,4,4,3,1, 1,  2,2,0,0,  0,0,0,   0,0,  2,    4,   2},
                            {1,3,4,6,3,1, 1,  2,2,2,1,  0,0,0,   0,0,  0,    0,   0,     1,      1,       1,       4,    4},
                            {1,3,2,6,0,0, 1,  2,2,0,0,  5,3,1,   2,1},
                            {1,3,2,6,0,0, 1,  2,2,0,0,  1,1,1,   5,4}};
    static byte[][] hoursTemp={{},{},{},{}};
    static byte[][] hoursSaved={{},{},{},{}};


    static StringBuilder sb = new StringBuilder();



    public static void main(String[] args){
        ArrayList<String> resultArray = new ArrayList<>();
        ArrayList<byte[]> codeArray = new ArrayList<>();
        ArrayList<byte[]> codeArrayForEn = new ArrayList<>();
        int length = hours[3].length;
        //Предмет первой группы
        for (byte firstSubject = 0; firstSubject < length+2; firstSubject++) {
            if(hours[0][firstSubject] == 0) continue;

            //Предмет второй группы
            for (byte secondSubject = 0; secondSubject < length; secondSubject++) {
                if (hours[1][secondSubject] == 0 || secondSubject == firstSubject) continue;

                //Предмет третьей группы
                for (byte thirdSubject = 0; thirdSubject < length; thirdSubject++) {
                    if (hours[2][thirdSubject] == 0 || thirdSubject == secondSubject || thirdSubject == firstSubject) continue;
                    if (isDuoSubject(thirdSubject)){
                        resultArray.add(createResultString(firstSubject,secondSubject,thirdSubject));
                        codeArray.add(createCode(firstSubject,secondSubject,thirdSubject));
                        continue;
                    }
                    //Предмет четвёртой группы
                    for (byte fourthSubject = 0; fourthSubject < length; fourthSubject++) {
                        if (hours[3][fourthSubject] == 0 || fourthSubject == firstSubject  || fourthSubject == secondSubject || fourthSubject == thirdSubject) continue;
                        if (isDuoSubject(fourthSubject)) continue;
                        resultArray.add(createResultString(firstSubject,secondSubject,thirdSubject,fourthSubject));
                        codeArray.add(createCode(firstSubject,secondSubject,thirdSubject,fourthSubject));
                    }
                }
            }
        }
        //частные случаи с языками и группами информатики
        for (byte thirdSubject = 0; thirdSubject < length; thirdSubject++) {
            if (hours[2][thirdSubject] == 0 ) continue;
            if (isDuoSubject(thirdSubject)){
                resultArray.add(createResultString("Фр+Нем","АВ1+АВ4",subjectNames.get(thirdSubject)));
                codeArray.add(createCode((byte)18,(byte)19,thirdSubject));
                resultArray.add(createResultString("Фр+Нем","АВ1+2Инф1",subjectNames.get(thirdSubject)));
                codeArray.add(createCode((byte)18,(byte)20,thirdSubject));
                continue;
            }
            //Предмет четвёртой группы
            for (byte fourthSubject = 0; fourthSubject < length; fourthSubject++) {
                if (hours[3][fourthSubject] == 0 || fourthSubject == thirdSubject) continue;
                if (isDuoSubject(fourthSubject)) continue;
                resultArray.add(createResultString("Фр+Нем","АВ1+АВ4",subjectNames.get(thirdSubject), subjectNames.get(fourthSubject)));
                codeArray.add(createCode((byte)18,(byte)19,thirdSubject,fourthSubject));
                if(thirdSubject == length || fourthSubject == length) continue;
                resultArray.add(createResultString("Фр+Нем","АВ1+2Инф1",subjectNames.get(thirdSubject), subjectNames.get(fourthSubject)));
                codeArray.add(createCode((byte)18,(byte)20,thirdSubject,fourthSubject));
            }
        }
        //частные случаи с расходящейся первой группой, убраны 3 последних предмета, так как они конфликтуют с остальными группами
        for (byte firstSubject = 0; firstSubject < length-1; firstSubject++) {
            if(hours[0][firstSubject] == 0) continue;

            //Предмет третьей группы
            for (byte thirdSubject = 0; thirdSubject < length-1; thirdSubject++) {
                if (hours[2][thirdSubject] == 0 || thirdSubject == firstSubject) continue;

                if (isTrioSubject(thirdSubject)){
                    resultArray.add(createResultString(subjectNames.get(firstSubject),"2(А1+А6)4",subjectNames.get(thirdSubject)));
                    codeArrayForEn.add(createCode(firstSubject,(byte)22,thirdSubject));
                    resultArray.add(createResultString(subjectNames.get(firstSubject),"2(А3+А4)4",subjectNames.get(thirdSubject)));
                    codeArrayForEn.add(createCode(firstSubject,(byte)23,thirdSubject));
                }
                if (isDuoSubject(thirdSubject)){
                    resultArray.add(createResultString(subjectNames.get(firstSubject),"1Инф1+АВ4",subjectNames.get(thirdSubject)));
                    codeArray.add(createCode(firstSubject,(byte)21,thirdSubject));
                    continue;
                }
                //Предмет четвёртой группы
                for (byte fourthSubject = 0; fourthSubject < length-1; fourthSubject++) {
                    if (hours[3][fourthSubject] == 0 || fourthSubject == firstSubject  || fourthSubject == thirdSubject) continue;
                    if (isDuoSubject(fourthSubject)) continue;
                    resultArray.add(createResultString(subjectNames.get(firstSubject),"1Инф1+АВ4",subjectNames.get(thirdSubject),subjectNames.get(fourthSubject)));
                    codeArray.add(createCode(firstSubject,(byte)21,thirdSubject,fourthSubject));
                }
            }
        }


        System.out.println(resultArray.size());
        System.out.println(codeArray.size() +codeArrayForEn.size());

        try {
            File file = new File("output.txt");
            FileWriter writer = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(writer, 8192);
            for (String str:resultArray) {
                bufferedWriter.write(str.concat("\n"));
            }
            bufferedWriter.flush();
            bufferedWriter.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            File file = new File("outputSet.txt");
            FileWriter writer = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(writer, 8192);
            for (int i = 0; i < 3; i++) {
                bufferedWriter.write("SET #"+(i+1)+"\n");
                for (String str:generateSetExample(codeArray,codeArrayForEn)) {
                    bufferedWriter.write(str.concat("\n"));
                }
            }
            bufferedWriter.flush();
            bufferedWriter.close();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //для общих случаев, отсутствуют лишние проверки.
    //для случаев с тремя уроками
    private static String createResultString(int firstSubject, int secondSubject, int thirdSubject){
        sb.delete(0,sb.length());
        sb.append("1").append(subjectNames.get(firstSubject)).append(hours[0][firstSubject]).append("+2")
                .append(subjectNames.get(secondSubject)).append(hours[1][secondSubject]).append("+3")
                .append(subjectNames.get(thirdSubject)).append(hours[2][thirdSubject]);
        return sb.toString();
    }
    //для общих случаев, отсутствуют лишние проверки.
    //для случаев с четырьмя уроками
    private static String createResultString(int firstSubject, int secondSubject, int thirdSubject, int fourthSubject){
        createResultString(firstSubject,secondSubject,thirdSubject);
        sb.append("+4").append(subjectNames.get(fourthSubject)).append(hours[3][fourthSubject]);
        return sb.toString();
    }
    //для любых случаев, лишние проверки
    //для случаев с тремя уроками
    private static String createResultString(String firstSubject, String secondSubject, String thirdSubject) {
        sb.delete(0, sb.length());
        if (subjectNames.contains(firstSubject))
            sb.append("1").append(firstSubject).append(hours[0][subjectNames.indexOf(firstSubject)]);
        else
            sb.append(firstSubject);
        if (subjectNames.contains(secondSubject))
            sb.append("+2").append(secondSubject).append(hours[1][subjectNames.indexOf(secondSubject)]);
        else
            sb.append("+".concat(secondSubject));
        if (subjectNames.contains(thirdSubject))
            sb.append("+3").append(thirdSubject).append(hours[2][subjectNames.indexOf(thirdSubject)]);
        else
            sb.append("+".concat(thirdSubject));
        return sb.toString();
    }
    //для любых случаев, лишние проверки
    //для случаев с четырьмя уроками
    private static String createResultString(String firstSubject, String secondSubject, String thirdSubject, String fourthSubject){
        createResultString(firstSubject,secondSubject,thirdSubject);
            if(subjectNames.contains(fourthSubject))
                sb.append("+4").append(fourthSubject).append(hours[3][subjectNames.indexOf(fourthSubject)]);
            else
                sb.append("+".concat(fourthSubject));
        return sb.toString();
    }
    //Предикат определяющий, является ли предмет совместимым для 3 и 4 группы
    private static boolean isDuoSubject(int number){
        return duoSubjectNumbers.contains(number);
    }
    //Предикат определяющий, является ли предмет совместимым для 2, 3 и 4 группы
    private static boolean isTrioSubject(int number){
        return trioSubjectNumbers.contains(number);
    }
    //Создаёт массив байт с номерами предметов для 4 предметов за час
    private static byte[] createCode(byte first,byte second,byte third,byte forth){
        return new byte[] {first,second,third,forth};
    };
    //Создаёт массив байт с номерами предметов для 3 предметов за час
    private static byte[] createCode(byte first,byte second,byte third){
        return new byte[] {first,second,third};
    };


    //Данный метод включает в себя раздельный вызов генерации занятий с английским для 2,3,4 групп и вызов генерации занятий с остальными предметами
    //Обособленность генерации английского обусловлена необходимостью генерировать его первым.
    //Генерировать его первым необходимо потому что он требует занятий с предметами смежными сразу для трёх групп
    //Так же его генерация обособлена иным способом добавления в список занятий. Так наборы предметов частично зависимы от уже сгенерированного.
    //Тоесть, если мы генерируем для групп английского А1+А6 занятие по типу: "ПС+А1+А6+М6" где общая для трёх групп математика, то необходимо
    //добавить еще одну строку с "*+А3+А4+М6", где *- любой доступный предмет для первой группы. Это необходимо для того, чтобы все люди учащиеся
    //в группах 2,3,4 побывали одинаковое число часов на смежных предметах, и за 2 такие строки у этих групп закрывается 1 час английского и 1 час математики
    // а у первой группы 2 часа любых доступных предметов, что говорит о ненарушении условий полной занятости студентов.
    private static ArrayList<String> generateSetExample(ArrayList<byte[]> codeArray, ArrayList<byte[]> codeArrayForEn){
        ArrayList<byte[]> codeArrayCopy = new ArrayList<>(codeArray);
        ArrayList<byte[]> codeArrayForEnCopy = new ArrayList<>(codeArrayForEn);
        ArrayList<String> resultSetExample = new ArrayList<>();

        resetHours();
        //Добавляем английский
        for (byte[] arr:chooseEnglish(codeArrayForEnCopy)) {
            resultSetExample.add(createResultString(arr[0],arr[1],arr[2]));
        }
        for (byte[] arr:chooseSubjects(codeArrayCopy)) {
            if(arr.length ==3 )resultSetExample.add(createResultString(arr[0],arr[1],arr[2]));
            else resultSetExample.add(createResultString(arr[0],arr[1],arr[2],arr[3]));
        }
        return resultSetExample;
    }

    //В данном методе реализована система перезапуска метода, так как при полностью случайном подборе комбинаций уроков
    //построенный сет может зайти в тупиковую ситуацию, например у 1г остался 1 час русского, у 2г остался 1 час математики,
    //у 3г и 4г осталось по одному часу физики(но у групп разные программы и потому нельзя совместить). Все тупиковые ситуации
    //основаны на том, что остаются одинаковые предметы которые невозможно одновременно проводить.
    //Поэтому при входе в тупиковую ситуаци, а именно когда остались часы у групп, но не осталось комбинаций, метод перезапускается
    // и снова пытается подобрать комбинацию, и так до тех пор пока не найдет решение.
    //количество тупиковых ситуаций выводит как failcounter = x. просто для статистики
    private static ArrayList<byte[]> chooseSubjects(ArrayList<byte[]> codeArray){
        int failcounter=0;
        ArrayList<byte[]> resultArray = new ArrayList<>();
        saveHours();
        ArrayList<byte[]> codeArrayTemp = new ArrayList<>(codeArray);
        codeArrayTemp = thinCodeArray(codeArrayTemp);
        byte[] res;
        while (!allHoursNull()){
            if(codeArrayTemp.isEmpty()){
                failcounter++;
                loadHours();
                codeArrayTemp = new ArrayList<>(codeArray);
                resultArray.clear();
            }
            //выбираем случайную строку, убираем часы и убираем лишние строки
            res = codeArrayTemp.get((int)Math.round(Math.random()*(codeArrayTemp.size()-1)));
            hoursTemp[0][res[0]]--;
            hoursTemp[1][res[1]]--;
            hoursTemp[2][res[2]]--;
            if(res.length == 3) hoursTemp[3][res[2]]--;
            else hoursTemp[3][res[3]]--;
            resultArray.add(res.clone());
            codeArrayTemp = thinCodeArray(codeArrayTemp);
        }
        System.out.println("failcounter = "+failcounter);
        return resultArray;
    };

    private static ArrayList<byte[]> chooseEnglish(ArrayList<byte[]> codeArrayForEn){
        ArrayList<byte[]> resultArray = new ArrayList<>();

        //4 часа английского для 2,3 и 4 группы выходят в 8 часов обычных занятий в 1 группе.
        for (int i = 0; i < 4; i++) {
            //выбираем случайную возможную строку и уменьшаем часы в списке и добавляем строку с противоположными группами английского
            //и любым возможным предметом первой группы
            byte[] res = codeArrayForEn.get((int)Math.round(Math.random()*codeArrayForEn.size()));
            hoursTemp[0][res[0]]--;
            hoursTemp[1][res[1]]--;
            hoursTemp[1][res[2]]--;
            hoursTemp[2][res[2]]--;
            hoursTemp[3][res[2]]--;
            resultArray.add(res.clone());
            if(res[1]==22) res[1]=23;
            else res[1]=22;
            codeArrayForEn = thinCodeArray(codeArrayForEn);
            res[0] = codeArrayForEn.get((int)Math.round(Math.random()*codeArrayForEn.size()))[0];
            hoursTemp[0][res[0]]--;
            hoursTemp[1][res[1]]--;
            resultArray.add(res.clone());
            codeArrayForEn = thinCodeArray(codeArrayForEn);
        }
        return resultArray;
    };

    private static boolean allHoursNull(){
        for (int i = 0; i < hoursTemp.length; i++) {
            for (int j = 0; j < hoursTemp[i].length; j++) {
                if (hoursTemp[i][j] > 0) return false;
            }
        }
        return true;
    }

    private static boolean isNeed(byte[] arr){
        if(hoursTemp[0][arr[0]] == 0) return false;
        if(hoursTemp[1][arr[1]] == 0) return false;
        if(hoursTemp[2][arr[2]] == 0) return false;
        if(arr.length == 3){
            if(hoursTemp[3][arr[2]] == 0) return false;
        }
        else if(hoursTemp[3][arr[3]] == 0) return false;
        return true;
    }

    private static ArrayList<byte[]> thinCodeArray(ArrayList<byte[]> codeArray){
        ArrayList<byte[]>  codeArrayCop =new ArrayList<>();
        for (byte[] arr:codeArray) {
            if(isNeed(arr)){ codeArrayCop.add(arr);}
        }
        return codeArrayCop;
    }

    private static void resetHours(){
        hoursTemp[0] = hours[0].clone();
        hoursTemp[1] = hours[1].clone();
        hoursTemp[2] = hours[2].clone();
        hoursTemp[3] = hours[3].clone();
    }
    private static void saveHours(){
        hoursSaved[0] = hoursTemp[0].clone();
        hoursSaved[1] = hoursTemp[1].clone();
        hoursSaved[2] = hoursTemp[2].clone();
        hoursSaved[3] = hoursTemp[3].clone();
    }
    private static void loadHours(){
        hoursTemp[0] = hoursSaved[0].clone();
        hoursTemp[1] = hoursSaved[1].clone();
        hoursTemp[2] = hoursSaved[2].clone();
        hoursTemp[3] = hoursSaved[3].clone();
    }





}
